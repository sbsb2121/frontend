const darkTheme = require("@ant-design/dark-theme");
const {
  addBundleVisualizer,
  addLessLoader,
  addWebpackAlias,
  fixBabelImports,
  override,
  useEslintRc
} = require("customize-cra");
const path = require("path");

const theme = darkTheme.default;
theme["@body-background"] = "#262626";
theme["@menu-highlight-color"] = "#3889EA";

const addSha = config => {
  const gitSha = process.env.REACT_APP_GIT_SHA || "0000000";
  let minifiedCSS = false;

  // eslint-disable-next-line no-unused-vars
  for (const plugin of config.plugins) {
    try {
      if (
        typeof plugin.options.chunkFilename === "string" &&
        typeof plugin.options.filename === "string"
      ) {
        plugin.options.filename = `static/css/[name].${gitSha}.css`;
        plugin.options.chunkFilename = `static/css/[name].${gitSha}.chunk.css`;
        minifiedCSS = true;
      }
    } catch (_) {}
  }

  if (minifiedCSS) {
    config.output.chunkFilename = `static/js/[name].${gitSha}.chunk.js`;
    config.output.filename = `static/js/[name].${gitSha}.js`;
  }

  return config;
};

module.exports = override(
  addBundleVisualizer({}, true),
  config => addSha(config),
  addLessLoader({
    javascriptEnabled: true,
    modifyVars: theme
  }),
  addWebpackAlias({
    "@ant-design/icons/lib/dist$": path.resolve(
      __dirname,
      "./webpackhooks/icons.js"
    )
  }),
  addWebpackAlias({
    moment: path.resolve(__dirname, "./webpackhooks/moment.js")
  }),
  fixBabelImports("import", {
    libraryDirectory: "es",
    libraryName: "antd",
    style: true
  }),
  useEslintRc()
);
