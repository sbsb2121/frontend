import { inject, observer } from "mobx-react";
import React, { Component } from "react";
import LazyLoad from "react-lazyload";
import Gallery, { RenderImageProps } from "react-photo-gallery";

import { IStores } from "../../CApp";

interface CLightViewProps {
  stores?: IStores;
}

@inject("stores")
@observer
export default class CLightView extends Component<CLightViewProps> {
  private readonly store = this.props.stores!.album;

  private imageRenderer(args: RenderImageProps): JSX.Element {
    return (
      <div
        key={args.photo.src}
        style={{
          margin: args.margin,
          overflow: "hidden",
          position: "relative"
        }}
      >
        <LazyLoad height={args.photo.height} resize throttle={200}>
          <a
            href={args.photo.src}
            rel="noopener noreferrer"
            target="_blank"
            title={args.photo.alt}
          >
            <img
              alt={args.photo.alt}
              src={args.photo.src}
              style={{ height: args.photo.height, width: args.photo.width }}
            />
          </a>
        </LazyLoad>
      </div>
    );
  }

  render() {
    return (
      <Gallery
        photos={this.store.album.entries
          .filter(value => value.dimensions !== undefined) // TODO remove later
          .map(value => {
            return {
              alt: value.title,
              height: value.dimensions!.height,
              src: `https://s.put.re/${value.fileId}`,
              width: value.dimensions!.width
            };
          })}
        renderImage={this.imageRenderer}
      />
    );
  }
}
