import { List, Typography } from "antd";
import { inject, Observer, observer } from "mobx-react";
import React, { Component } from "react";
import { SortableContainer, SortableElement } from "react-sortable-hoc";

import { IStores } from "../../CApp";
import { AlbumEntry } from "../../stores/CAlbumStore";
import CCard from "./CCard";

const SortableListItem = SortableElement(List.Item);
const SortableList = SortableContainer(List);

interface CGridViewProps {
  stores?: IStores;
}

@inject("stores")
@observer
export default class CGridView extends Component<CGridViewProps> {
  private readonly store = this.props.stores!.album;

  render() {
    return (
      <SortableList
        axis="xy"
        dataSource={this.store.album.entries}
        distance={1} // Do not interrupt CCard actions
        grid={{ gutter: 16, xs: 1, sm: 2, md: 2, lg: 2, xl: 3, xxl: 4 }}
        pagination={{
          defaultCurrent: 1,
          defaultPageSize: 8,
          hideOnSinglePage: true,
          total: this.store.album.entries.length
        }}
        renderItem={(item, index) => {
          const file = item as AlbumEntry;

          return (
            <Observer>
              {() => {
                if (!file.dimensions) {
                  // TODO: Remove
                  return <span />;
                }

                return (
                  <SortableListItem
                    key={file.fileId}
                    disabled={!this.store.canEdit}
                    index={index}
                  >
                    <CCard
                      alt={file.title}
                      description={
                        <Typography.Text
                          editable={
                            this.store.canEdit && {
                              editing: this.store.editing === `${index}d`,
                              onStart: () => (this.store.editing = `${index}d`),
                              onChange: value =>
                                this.store.setFileDescription(index, value)
                            }
                          }
                        >
                          {file.description}
                        </Typography.Text>
                      }
                      editable={this.store.canEdit}
                      selected={this.store.selectedEntries.indexOf(index) > -1}
                      src={`https://s.put.re/${file.fileId}`}
                      title={
                        <Typography.Text
                          editable={
                            this.store.canEdit && {
                              editing: this.store.editing === `${index}t`,
                              onStart: () => (this.store.editing = `${index}t`),
                              onChange: value =>
                                this.store.setFileTitle(index, value)
                            }
                          }
                        >
                          {file.title}
                        </Typography.Text>
                      }
                      onDelete={() => this.store.deleteEntry(index)}
                      onSelectChange={() =>
                        this.store.toggleSelectedEntry(index)
                      }
                    />
                  </SortableListItem>
                );
              }}
            </Observer>
          );
        }}
        onSortEnd={e => this.store.swapEntries(e.oldIndex, e.newIndex)}
      />
    );
  }
}
