import "./CPlayer.css";

import { Button, Col, Divider, Row, Typography } from "antd";
import queryString from "query-string";
import React, { Component } from "react";
import ReactPlayer from "react-player";
import { RouteComponentProps } from "react-router-dom";

import extractMatches from "../utils/extractMatches";

interface CPlayerProps extends RouteComponentProps<{ videoId: string }> {
  state?: CPlayerState;
}

export interface CPlayerState {
  originalName: string;
  url: string;
}

export default class CPlayer extends Component<CPlayerProps> {
  readonly state: CPlayerState = { originalName: "", url: "" };

  async componentDidMount() {
    if (this.props.state) {
      this.setState(this.props.state);
      return;
    }

    const params = queryString.parse(this.props.location.search);
    const matches = await extractMatches(
      (params.id as string) || this.props.match.params.videoId
    );

    if (
      matches.length === 0 ||
      matches[0].metadata.mimeType.split("/")[0] !== "video"
    ) {
      return this.props.history.push("/404-not-found");
    }

    this.setState({
      originalName: matches[0].metadata.originalName,
      url: `https://s.put.re/${matches[0].fileId}`
    });
  }

  render() {
    return (
      <>
        <Row>
          <Col>
            <Typography.Title>{this.state.originalName}</Typography.Title>
          </Col>
        </Row>

        <Row>
          <Col>
            <Button
              icon="download"
              onClick={() => window.open(this.state.url, "_blank")}
            >
              Download
            </Button>
          </Col>
        </Row>

        <Row>
          <Col>
            <Divider />
          </Col>
        </Row>

        <Row>
          <Col>
            <div className="player-wrapper">
              <ReactPlayer
                className="react-player"
                controls
                height="100%"
                url={this.state.url}
                width="100%"
              />
            </div>
          </Col>
        </Row>
      </>
    );
  }
}
